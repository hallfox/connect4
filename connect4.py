"""This is a sane implementation of Connect 4, targeted towards what a
beginner might do. No classes or crazy module tricks.

"""

ROWS = 6
COLS = 7
BLANK = ' '
RED = 'R'
YELLOW = 'Y'

### MODEL
def new_game():
    return {
        "board": [BLANK] * (ROWS * COLS),
        "turn": RED,
        "moves": [i for i in range(COLS)],
        "game_over": False,
        "winner": BLANK,
    }

def get_piece(board, row, col):
    pos = col + row * COLS
    if 0 <= pos and pos < ROWS * COLS:
        return board[pos]
    return BLANK

def set_piece(board, row, col, piece):
    pos = col + row * COLS
    if 0 <= pos and pos < ROWS * COLS:
        board[pos] = piece

def get_board(game):
    return game["board"]

def get_turn(game):
    return game["turn"]

def get_moves(game):
    return game["moves"]

def get_winner(game):
    return game["winner"]

def get_top(board, col):
    return board[col]

def is_blank(piece):
    return piece == BLANK

def is_game_over(game):
    return game["game_over"]

def switch_turns(game):
    if game["turn"] == RED:
        game["turn"] = YELLOW
    else:
        game["turn"] = RED

def slots(board, col):
    return [i for i in range(ROWS - 1, -1, -1)]

def win(game, turn):
    game["winner"] = turn
    game["game_over"] = True

def intersecting_fours(board, row, col):
    intersections = [[get_piece(board, row+i, col) for i in range(4)]]
    for offset in range(4):
        horiz = [get_piece(board, row, col+i-offset) for i in range(4)]
        diag = [get_piece(board, row-i+offset, col+i-offset) for i in range(4)]
        backdiag = [get_piece(board, row+i-offset, col+i-offset) for i in range(4)]
        intersections.extend([horiz, diag, backdiag])
    return intersections

### VIEW
def draw_board(game):
    board = get_board(game)
    for row in range(ROWS):
        for col in range(COLS):
            print("|", get_piece(board, row, col), "|", sep="", end="")
        print()

def draw_slot_ids(game):
    for move in get_moves(game):
        print(f" {move+1} ", sep="", end="")
    print()

def draw_game(game):
    draw_board(game)
    draw_slot_ids(game)

### CONTROLLER
def has_room(game, move):
     board = get_board(game)
     top = get_top(board, move)
     return is_blank(top)

def is_valid_move(game, move):
    moves = get_moves(game)
    if move in moves and has_room(game, move):
        return True
    else:
        return False

def all_same(game, pieces):
    turn = get_turn(game)
    for piece in pieces:
        if turn != piece:
            return False
    return True

def prompt_move(game):
    turn = get_turn(game)
    while True:
        move = int(input(f"{turn}'s turn. Where would you like to put your piece? ")) - 1
        if is_valid_move(game, move):
            return move
        else:
            print(f"Invalid move: {move+1}")

def make_move(game, move):
    board = get_board(game)
    turn = get_turn(game)
    for slot in slots(board, move):
        if is_blank(get_piece(board, slot, move)):
            set_piece(board, slot, move, turn)
            return [slot, move]

def check_win(game, row, col):
    board = get_board(game)
    turn = get_turn(game)
    for fours in intersecting_fours(board, row, col):
        if all_same(game, fours):
            win(game, turn)
            return

def play_turn(game):
    draw_game(game)
    move = prompt_move(game)
    move_position = make_move(game, move)
    check_win(game, move_position[0], move_position[1])

def main():
    print("Connect Four!")
    game = new_game()
    while not is_game_over(game):
        play_turn(game)
        switch_turns(game)
    winner = get_winner(game)
    print(f"{winner} wins!")
    draw_board(game)
    print("Thanks for playing!")

if __name__ == "__main__":
    main()
